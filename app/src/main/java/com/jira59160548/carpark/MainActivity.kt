package com.jira59160548.carpark

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.jira59160548.carpark.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private var ParkList: List<Park> = listOf()
    private lateinit var binding: ActivityMainBinding
    private lateinit var park: Park
    private var index: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        val num = 3
        for (i in 1..num) {
            ParkList += Park()
        }
        setListeners()
        park = ParkList[index]
        setParkValue(park = this.park)

        binding.apply {
            updateButton.setOnClickListener { updateParkValue(it) }
            doneButton.setOnClickListener { doneParkValue(it) }
            deleteButton.setOnClickListener { deleteParkValue() }
        }
    }

    private fun getParkSelected(view: View) {
        when (view.id) {
            R.id.park1_button -> { index = 0 }
            R.id.park2_button -> { index = 1 }
            R.id.park3_button -> { index = 2 }
            else -> this.park = Park()
        }
        this.park = ParkList[index]
        checkEmptyParkIndex()
        setParkValue(park)
    }

    private fun checkEmptyParkIndex() {
        if (ParkList[index].license != "ว่าง") {
            binding.updateButton.visibility = View.GONE
            binding.deleteButton.visibility = View.VISIBLE
        } else {
            binding.updateButton.visibility = View.VISIBLE
            binding.deleteButton.visibility = View.GONE
        }
    }


    private fun setParkValue(park: Park) {
        binding.apply {
            invalidateAll()
            licenseText.text = park.license
            brandText.text = park.brand
            ownerNameText.text = park.ownerName
        }
    }

    private fun updateParkValue(view: View) {
        binding.apply {
            licenseEditText.setText(park.license)
            brandEditText.setText(park.brand)
            ownerNameEditText.setText(park.ownerName)

            licenseText.visibility = View.GONE
            brandText.visibility = View.GONE
            ownerNameText.visibility = View.GONE

            licenseEditText.visibility = View.VISIBLE
            brandEditText.visibility = View.VISIBLE
            ownerNameEditText.visibility = View.VISIBLE

            view.visibility = View.GONE
            doneButton.visibility = View.VISIBLE
        }

    }

    private fun doneParkValue(view: View) {
        binding.apply {
            park.license =  licenseEditText.text.toString()
            park.brand =  brandEditText.text.toString()
            park.ownerName =  ownerNameEditText.text.toString()
            setParkValue(park)

            licenseText.visibility = View.VISIBLE
            brandText.visibility = View.VISIBLE
            ownerNameText.visibility = View.VISIBLE

            licenseEditText.visibility = View.GONE
            brandEditText.visibility = View.GONE
            ownerNameEditText.visibility = View.GONE

            view.visibility = View.GONE
            updateButton.visibility = View.VISIBLE
        }
        updateButtonValue()
        checkEmptyParkIndex()
//        bindButtonColor()
    }

//    private fun bindButtonColor() {
//        binding.apply {
//            when (index) {
//                0 -> {
//                    setButtonColor(park1Button)
//                }
//                1 -> {
//                    setButtonColor(park2Button)
//                }
//                2 -> {
//                    setButtonColor(park3Button)
//                }
//            }
//        }
//    }
//
//    private fun setButtonColor(button: Button) {
//        if(button.text == "ว่าง") {
//            button.setBackgroundColor(Color.parseColor("#dd2c00"))
//        } else {
//            button.setBackgroundColor(Color.parseColor("#ff0000"))
//        }
//    }

    private fun updateButtonValue() {
        binding.apply {
            when (index) {
                0 -> {
                    park1Button.text = park.license
                }
                1 -> {
                    park2Button.text = park.license
                }
                2 -> {
                    park3Button.text = park.license
                }
            }
        }
    }

    private fun deleteParkValue() {
        binding.apply {
            ParkList[index].clearPark()
            park = ParkList[index]
            setParkValue(park)
        }
        updateButtonValue()
        checkEmptyParkIndex()
//        bindButtonColor()
    }

    private fun setListeners() {
        binding.apply {
            var clickableView: List<View> = listOf(park1Button, park2Button, park3Button)
            for (view in clickableView) {
                view.setOnClickListener{ getParkSelected(it) }
            }
        }
    }

}
